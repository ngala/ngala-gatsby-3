/**
 * @type {import('gatsby').GatsbyConfig}
 */
module.exports = {
  pathPrefix: `/ngala-gatsby-3`,
  siteMetadata: {
    title: `Ngala Gatsby 3`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  plugins: [],
}
